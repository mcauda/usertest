<?php
// Routes

$app->get('/profile/facebook/{userId}', function ($request, $response, $args) {
    $userId = $args['userId'];

    $fb = new Facebook\Facebook([
        'app_id' => $this->get('settings')['facebook']['app_id'],
        'app_secret' => $this->get('settings')['facebook']['app_secret'],
        'default_graph_version' => 'v2.8',
    ]);
    $accessToken = $this->get('settings')['facebook']['app_id'] . '|' . $this->get('settings')['facebook']['app_secret'];

    try {
        $result = $fb->get('/'.$userId, $accessToken);
    } catch(\Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(\Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $user = $result->getGraphUser();

    $return = array('id' => $userId,
                    'name' => $user->getName(),
        );

    return $response->withStatus(200)->write(json_encode($return));
});
